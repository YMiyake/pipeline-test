FROM python:3.6

COPY ./requirements.txt requirements.txt
#RUN apt-get update
#RUN apt-get install  libmecab2 libmecab-dev mecab mecab-ipadic mecab-ipadic-utf8 mecab-utils
RUN pip install -r requirements.txt
